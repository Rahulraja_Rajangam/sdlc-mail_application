const express=require('express');
const loginRouter = require("./router/loginRouter");
const mailRouter = require("./router/mailRouter");
const path = require("path");
const cors =require("cors")

const app=express();

app.use(express.static( path.join(__dirname, './build')));
app.use(express.urlencoded({extended:false}))
app.use("/", loginRouter);
app.use("/mail", mailRouter);
app.use(express.json())
app.use(cors())


app.listen(3000,()=>{
    console.log("server running on port 3000");
})
